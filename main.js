let arr = ['hello', 'world', [1, 5, 9],'Kiev', 'Kharkiv', 'Odessa', 'Lviv'];

let lists = document.getElementById("lists");

function creatList(arr) {
    let ul = document.createElement('ul');
    let newArr = arr.map(function (elem) {
        let li = document.createElement("li");
        let newList;
        if (Array.isArray(elem)) {
            newList = creatList(elem);
        } else if (typeof elem === 'object' && !Array.isArray(elem)) {
            let dopList = Object.keys(elem);
            newList = creatList(dopList)
        } else newList = document.createTextNode(elem);

        li.appendChild(newList);
        ul.appendChild(li);
    });
    lists.appendChild(ul);
    return ul
}

creatList(arr);

// function creatListOfElements(arr) {
//     for (let i = 0; i < arr.length; i++) {
//         const newLi = document.createElement("li");
//         newLi.innerHTML = arr[i];
//         list.appendChild(newLi);
//
//     }
// }
